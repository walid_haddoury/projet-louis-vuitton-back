import * as admin from "firebase-admin";
import {key} from "./key";


admin.initializeApp({
    credential: admin.credential.cert(key as any)
});

