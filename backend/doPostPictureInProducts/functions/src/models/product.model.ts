export interface ProductModel {

  uid: any;
  ref: any;
  title: string;
  historyUid: string;
  creationDate: Date;
  ownerUid: string;
  pictureUid: string;
  videoUid: string;
  locationUid: string;

}
