export interface StoriesModel {
    uid: any;
    ref: any;
    language: languageEnum;
    status: string;
    creationDate: Date;
    modificationDate: Date;
    text: string;
    title: string;
    productUid: string;
}

export enum languageEnum {french = 'french', english = 'english', deutsch = 'deutsch', spanish = 'spanish', japanese = 'japanese'}
