import * as admin from 'firebase-admin';
import {key} from "./key";
import {StoriesModel} from "./models/stories.model";



admin.initializeApp({
    credential: admin.credential.cert(key as any)
});
const db = admin.firestore();




export async function pushStories(addStories: StoriesModel): Promise<FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>> {
    return db
        .collection('stories')
        .add(addStories).then(async ref => {
            await ref.set({uid: ref.id}, {merge: true});
            return ref;
        });
}
