import * as admin from "firebase-admin";
import {key} from "./key";
import QuerySnapshot = admin.firestore.QuerySnapshot;
import {ProductModel} from "./models/product.model";



admin.initializeApp({
    credential: admin.credential.cert(key as any)
});
const db = admin.firestore();

export async function getProducts()  {
    const productsList: QuerySnapshot = await db
        .collection('products')
        .get();
    const listOfProducts: ProductModel[] = [];
    productsList.forEach(result =>
        listOfProducts.push(result.data() as ProductModel));
    return listOfProducts;
}
