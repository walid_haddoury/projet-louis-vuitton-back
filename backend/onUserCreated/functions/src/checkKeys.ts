// function pour voir si on a bien toutes les keys du formulaire d'inscription

import {UserModel} from "./models/users.model";

export function checkMail(user: UserModel) {
    return (Object.values(user.mail as string).some(value => value === "@"));
}
