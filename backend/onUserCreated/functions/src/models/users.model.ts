export interface UserModel {
    uid: any;
    lastName?: string;
    firstName?: string;
    mail?: string;
    phone?: string;
    mobile?: string;
    departement?: string;
    adress?: string;
    zipCode?: number;
    city?: string;
    country?: string;

    admin?: boolean;
    client?: boolean;
}