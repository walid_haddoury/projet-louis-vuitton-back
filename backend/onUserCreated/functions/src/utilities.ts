import * as admin from 'firebase-admin';
import {UserModel} from "./models/users.model";
import {key} from "./key";

admin.initializeApp({
    credential: admin.credential.cert(key as any)
});
const db = admin.firestore();

export async function addUser(userData: UserModel): Promise<FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>> {
    return db
        .collection('users')
        .add(userData).then(async ref => {
            await ref.set({uid: ref.id}, {merge: true});
            return ref;
        });
}