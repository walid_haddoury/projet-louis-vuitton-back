import * as functions from 'firebase-functions';
import {addUser} from "./utilities";
import * as express from "express";

const cors = require('cors');

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.post('', async function (req, res) {
    try {
        const newUser = req.body;
        const addTheNewUser = await addUser(newUser);
        return res.send(addTheNewUser);

    } catch (e) {
        return res.status(500).send(e);
    }
});

export const doCreateUser = functions.https.onRequest(app);
