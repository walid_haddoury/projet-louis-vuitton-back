import * as functions from 'firebase-functions';
import * as express from 'express';
import {deleteStoryId} from "./utilities";


const cors = require('cors');

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.delete('/:id', async function deleteStoryById(req, res) {
   try{
       const id = req.params.id;
       const deleteStory = await deleteStoryId(id);
       res.send(deleteStory)
   } catch (e) {
       res.status(500).send(e)

   }

});



 export const doDeleteStoryById= functions.https.onRequest(app);

