import * as admin from "firebase-admin";
import {key} from "./key-firebase";


admin.initializeApp({
    credential: admin.credential.cert(key as any)
});

const db = admin.firestore();

export async function deleteStoryId(id: string) {
    return await db.collection('stories')
        .doc(id)
        .delete()



}
