import * as functions from 'firebase-functions';
import * as express from 'express';
import {addPicture} from "./utilities";

const cors = require('cors');

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


app.post('', async function (req, res) {
    try{
        const newPicture = req.body;
        const createPicture = await addPicture(newPicture);
        res.send(createPicture);
    }catch (e) {
        res.status(500).send(e)
    }
});

export const doPostPicture = functions.https.onRequest(app);
