import * as admin from "firebase-admin";
import {key} from "./key";


admin.initializeApp({
    credential: admin.credential.cert(key as any)
});
const db = admin.firestore();

export async function addPicture(picture: any) {
    return db
        .collection('pictures')
        .add(picture).then(async ref => {
            await ref.set({uid: ref.id}, {merge: true});
            return ref;
        })
}


