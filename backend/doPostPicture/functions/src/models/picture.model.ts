export interface PictureModel {
    uid: any;
    ref: any;
    title: string;
    image: string;
    productUid: string;
}
