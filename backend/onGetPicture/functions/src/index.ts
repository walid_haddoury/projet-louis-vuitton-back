import * as functions from 'firebase-functions';
import * as express from "express";
import {getPicture} from "./utilities";
const cors = require('cors');
const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


app.get('', async function (req, res) {
    try {
        const result = await getPicture();
        console.log(result);
        await res.send(result);
    } catch (e) {
        res.status(500).send(e)
    }
});
export const doGetPictures = functions.https.onRequest(app);
