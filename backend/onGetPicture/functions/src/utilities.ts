 import * as admin from 'firebase-admin';
import {key} from "./key";
import QuerySnapshot = admin.firestore.QuerySnapshot;
import {PictureModel} from "./models/picture.model";

admin.initializeApp({
    credential: admin.credential.cert(key as any)
});
const db = admin.firestore();



export async function getPicture() {
    const pictureList: QuerySnapshot = await db
        .collection('pictures')
        .get();
    const listOfPictures: PictureModel[] = [];
    pictureList.forEach(result =>
        listOfPictures.push(result.data() as PictureModel));
    return listOfPictures;
}



