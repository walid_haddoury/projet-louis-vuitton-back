export interface OwnerModel {
    uid: any;
    ref: any;
    lastName: string;
    firstName: string;
    birth: Date;
    death: Date;
    nationality: string;
    gender: GenderEnum;
    job: string;
    ascendant: string;
    mariage: string;
    children: string;
    source: string;
    productUid: string;

}

export enum GenderEnum {femme = 'femme', homme = 'homme'};