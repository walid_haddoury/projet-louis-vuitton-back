import * as functions from 'firebase-functions';
import * as express from 'express';
import {key} from "./key-firebase";
import {OwnerModel} from "./models/owner.model";

import * as admin from 'firebase-admin';

const cors = require('cors');
const app = express();

admin.initializeApp({
    credential: admin.credential.cert(key as any)
});
const db = admin.firestore();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.get('/:id', async function (req, res) {
    try {
        const letter = req.params.id;
        const ownerSnap = await db.collection('owners').get();
        let ownerList: OwnerModel[] = [];
        ownerSnap.forEach(ownersSnap => ownerList.push(ownersSnap.data() as OwnerModel));
        ownerList = ownerList.filter(value => value.lastName.charAt(0) === letter);
        return res.send(ownerList);
    } catch (e) {
        return res.status(500).send(e);
    }
});

export const doGetOwnersByLetter = functions.https.onRequest(app);
