import * as admin from "firebase-admin";
import { key } from "./key";
import {ProductModel} from "./models/product.model";



admin.initializeApp({
    credential: admin.credential.cert(key as any)
});
const db = admin.firestore();

export async function addProduct(product: ProductModel) {
    return db
        .collection('products')
        .add(product)
}



