// tslint:disable-next-line:no-implicit-dependencies
import { DocumentReference } from "@google-cloud/firestore";

export interface ProductModel {
    uid: any;
    ref: any;
    title: string;
    historyRef: DocumentReference;
    historyUid: string;
    creationDate: Date;
    ownerUid: string;
    pictureRef: DocumentReference;
    pictureUid: string;
    videoRef: DocumentReference;
    videoUid: string;
    locationRef: DocumentReference;
    locationUid: string;
}
