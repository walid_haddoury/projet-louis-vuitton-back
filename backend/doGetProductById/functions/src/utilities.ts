import * as admin from "firebase-admin";
import {key} from "./key";





admin.initializeApp({
    credential: admin.credential.cert(key as any)
});
const db = admin.firestore();

export async function getProductById(id:string) {
    return await db
        .collection('products')
        .doc(id)
        .get();
}