import * as admin from 'firebase-admin';
import {key} from './key-firebase';
import {OwnerModel} from "./models/owner.model";
import QuerySnapshot = admin.firestore.QuerySnapshot;


admin.initializeApp({
    credential: admin.credential.cert(key as any)
});
const db = admin.firestore();

export async function getOwners() {
    const dataOwners: QuerySnapshot = await db
        .collection('owners')
        .get();
    const listOfOwners: OwnerModel[] = [];
    dataOwners.forEach(result =>
        listOfOwners.push(result.data() as OwnerModel));
    return listOfOwners;
}






