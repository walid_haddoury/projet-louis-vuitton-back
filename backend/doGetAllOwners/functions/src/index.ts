import * as functions from 'firebase-functions';
import * as express from 'express';
import {getOwners} from "./utilities";

const cors = require('cors');

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.get('', async function (req, res) {
    try {
        const allOwners = await getOwners();
        res.send(allOwners);
    } catch (e) {
        res.status(500).send(e);
    }
});

export const doGetAllOwners = functions.https.onRequest(app);