import * as functions from 'firebase-functions';
import * as express from "express";
import {getHistory, } from "./utilities";

const cors = require('cors');

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.get('/:id', async function (req, res) {
    try{
        const id = req.params.id;
        const result = await getHistory(id);
        res.send(result.data());
    }catch (e) {
        res.status(500).send(e)
    }
});

export const doGetStories = functions.https.onRequest(app);


