import * as admin from 'firebase-admin';
import {key} from "./key";

admin.initializeApp({
    credential: admin.credential.cert(key as any)
});
const db = admin.firestore();

export  function getHistory(id: string)  {
    return db
        .collection('stories')
        .doc(id)
        .get()
}