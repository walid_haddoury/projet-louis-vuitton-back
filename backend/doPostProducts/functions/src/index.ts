import * as functions from 'firebase-functions';
import * as express from 'express';
import {addProducts} from "./utilities";


const cors = require('cors');

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.post('', async function (req, res) {
    try{
        const newProducts = req.body;
        const createProducts = await addProducts(newProducts);
        res.send(createProducts);
    }catch (e) {
        res.status(500).send(e)
    }
});



 export const doPostPictures = functions.https.onRequest(app);
