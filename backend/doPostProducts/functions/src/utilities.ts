import {key} from "./key";
import * as admin from "firebase-admin";
import {ProductModel} from "./model/product.model";


admin.initializeApp({
    credential: admin.credential.cert(key as any)
});

const db = admin.firestore();


export async function addProducts(products: ProductModel) {
    return db
        .collection('products')
        .add(products).then(async ref => {
            await ref.set({uid: ref.id}, {merge: true});
            return ref;
        })
 }
