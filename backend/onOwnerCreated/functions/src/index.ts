import * as functions from 'firebase-functions';
import * as express from 'express';
import {addOwner, upperCase} from "./utilities";
const cors = require('cors');

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.post('', async function (req, res) {
   try {
      const newOwner = req.body;
      const toUpperCase = await upperCase(newOwner);
      console.log(toUpperCase)
      const result = await addOwner(toUpperCase);
      res.send(result);
   }catch (e) {
      res.status(500).send(e);
   }

});

export const doCreateOwner = functions.https.onRequest(app);
