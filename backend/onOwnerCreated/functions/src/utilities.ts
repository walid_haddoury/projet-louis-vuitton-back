import * as admin from 'firebase-admin';
import {OwnerModels} from "./models/owner.models";
import {key} from "./key";


admin.initializeApp({
    credential: admin.credential.cert(key as any)
});
const db = admin.firestore();

export function upperCase(ownerForm:OwnerModels) {
    ownerForm.lastName = ownerForm.lastName.toUpperCase();
    ownerForm.firstName = (ownerForm.firstName.charAt(0).toUpperCase() + ownerForm.firstName.slice(1));
    ownerForm.nationality = (ownerForm.nationality.charAt(0).toUpperCase() + ownerForm.nationality.slice(1));
    ownerForm.job = (ownerForm.job.charAt(0).toUpperCase() + ownerForm.job.slice(1));
    ownerForm.ascendant = (ownerForm.ascendant.charAt(0).toUpperCase() + ownerForm.ascendant.slice(1));
    ownerForm.mariage = (ownerForm.mariage.charAt(0).toUpperCase() + ownerForm.mariage.slice(1));
    ownerForm.children = (ownerForm.children.charAt(0).toUpperCase() + ownerForm.children.slice(1));
    ownerForm.source = (ownerForm.source.charAt(0).toUpperCase() + ownerForm.source.slice(1));
    return ownerForm;
}


export async function addOwner(ownerData: OwnerModels): Promise<FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>> {
    return db
        .collection('owners')
        .add(ownerData).then(async ref => {
            await ref.set({uid: ref.id}, {merge: true});
            return ref;
        });
}