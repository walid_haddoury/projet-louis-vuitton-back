export interface OwnerModels {
    uid: any;
    lastName: string;
    firstName: string;
    birth: Date;
    death: Date;
    nationality: string;
    gender: genderEnum;
    job: string;
    ascendant: string;
    mariage: string;
    children: string;
    source: string;
    productUid: string;

}

export enum genderEnum {femme = 'femme', homme = 'homme'};