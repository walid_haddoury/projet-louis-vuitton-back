import * as functions from 'firebase-functions';
import * as express from 'express';
import * as admin from 'firebase-admin';

const cors = require('cors');

const app = express();
import {key} from "./key-firebase";
import {OwnerModels} from "./models/owner.models";

admin.initializeApp({
    credential: admin.credential.cert(key as any)
});
const db = admin.firestore();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.get('', async function (req, res) {
    try {
        const ownerMap = new Map();
        const owner: OwnerModels[] = [];
        const ownerSnap = await db.collection('owners').get();
        ownerSnap.forEach(ownersSnap => owner.push(ownersSnap.data() as OwnerModels));
        owner.forEach(owners => ownerMap.set(
            owner && owners.lastName ? owners.lastName.charAt(0) : '',
            owner && owners.lastName ? owners.lastName.charAt(0).toUpperCase() : '')
        );
        return res.send(Array.from(ownerMap.values()).sort((a, b) => a < b ? -1 : 1));
    } catch (e) {
        return res.status(500).send(e);
    }
});

export const doGetAlphabeticalOwners = functions.https.onRequest(app);
